﻿using System;

namespace TransportProj
{
    public abstract class Car
    {
        public int XPos { get; protected set; }
        public int YPos { get; protected set; }
        public Passenger Passenger { get; private set; }
        public City City { get; private set; }

        public Car(int xPos, int yPos, City city, Passenger passenger)
        {
            XPos = xPos;
            YPos = yPos;
            City = city;
            Passenger = passenger;
        }

        protected virtual void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("Car moved to x - {0} y - {1}", XPos, YPos));
        }

        public void PickupPassenger(Passenger passenger)
        {
            Passenger = passenger;
        }

        public virtual void MoveUp(int destination)
        {
            if (YPos < City.YMax)
            {
                YPos++;
                WritePositionToConsole();
            }
        }

        public virtual void MoveDown(int destination)
        {
            if (YPos > 0)
            {
                YPos--;
                WritePositionToConsole();
            }
        }

        public virtual void MoveRight(int destination)
        {
            if (XPos < City.XMax)
            {
                XPos++;
                WritePositionToConsole();
            }
        }

        public virtual void MoveLeft(int destination)
        {
            if (XPos > 0)
            {
                XPos--;
                WritePositionToConsole();
            }
        }
    }
}
