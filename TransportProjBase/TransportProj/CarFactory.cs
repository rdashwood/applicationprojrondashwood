﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj
{
    class CarFactory
    {
        public static Car GetCar(int xPos, int yPos, Passenger passenger, City city)
        {
            Car car;
            if (((passenger.DestinationXPos - passenger.StartingXPos) > City.XMax/2) || ((passenger.DestinationYPos - passenger.StartingYPos) >
                City.YMax /2))
            {
                car= new Race(xPos, yPos, city, passenger);
            }
            else
            {
                car = new Sedan(xPos, yPos, city, passenger);
            }
            return car;
        }
    }
}
