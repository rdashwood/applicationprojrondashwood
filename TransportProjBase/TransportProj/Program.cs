﻿using System;

namespace TransportProj
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rand = new Random();
            int CityLength = 10;
            int CityWidth = 10;

            City MyCity = new City(CityLength, CityWidth);
            Passenger passenger = MyCity.AddPassengerToCity(rand.Next(CityLength - 1), rand.Next(CityWidth - 1), rand.Next(CityLength - 1), rand.Next(CityWidth - 1));
            Car car = MyCity.AddCarToCityBasedOnPassengerTripLength(rand.Next(CityLength - 1), rand.Next(CityWidth - 1),passenger);
            Console.WriteLine(String.Format("Passenger position is x - {0} y - {1}", passenger.StartingXPos,passenger.StartingYPos));
            while(!passenger.IsAtDestination())
            {
                Tick(car, passenger);
            }
            Console.WriteLine(String.Format("Car position is x - {0} y - {1}", car.XPos, car.YPos));
            Console.WriteLine(String.Format("Passenger destination  is x - {0} y - {1}", passenger.DestinationXPos, passenger.DestinationYPos));

            return;
        }

        /// <summary>
        /// Takes one action (move the car one spot or pick up the passenger).
        /// </summary>
        /// <param name="car">The car to move</param>
        /// <param name="passenger">The passenger to pick up</param>
        public static void Tick(Car car, Passenger passenger)
        {
            var isTickComplete = false;
            var tagetYPos = car.Passenger ==null ? passenger.StartingYPos: passenger.DestinationYPos;
            var isYMatch = car.YPos == tagetYPos;
            var targetXPos = car.Passenger == null ? passenger.StartingXPos : passenger.DestinationXPos;
            var isXMatch = car.XPos == targetXPos;

           if (isXMatch && isYMatch)
            {
                car.PickupPassenger(passenger);
                passenger.GetInCar(car);
                isTickComplete = true;
            }

            if (!isTickComplete && !isXMatch)
            {
                isTickComplete = true;
                MoveCarXPos(car, targetXPos);
            }

            if (!isTickComplete)
            {
                 MoveCarYPos(car, tagetYPos);
            }
           
  
        }

        private static void MoveCarYPos(Car car, int tagetYPos)
        {
            if (car.YPos < tagetYPos)
            {
                car.MoveUp(tagetYPos);
            }
            else
            {
                car.MoveDown(tagetYPos);
            }
        }

        private static void MoveCarXPos(Car car,  int targetXPos)
        {
            
            if (car.XPos < targetXPos)
            {
                car.MoveRight(targetXPos);
            }
            else
            {
                car.MoveLeft(targetXPos);
            }
            
        }
    }
}
