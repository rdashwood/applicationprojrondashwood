﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj
{
    class Race : Car
    {
        public Race(int xPos, int yPos, City city, Passenger passenger) : base(xPos, yPos, city, passenger)
        {
        }

        public override void MoveUp(int destination)
        {
            base.MoveUp(destination);
            if (YPos != destination)
            {
                base.MoveUp(destination);
            }

        }

        public override void MoveDown(int destination)
        {
            base.MoveDown(destination);
            if (YPos != destination)
            {
                base.MoveDown(destination);
            }
        }

        public override void MoveRight(int destination)
        {
            base.MoveRight(destination);
            if (XPos != destination)
            {
                base.MoveRight(destination);
            }
        }



        public override void MoveLeft(int destination)
        {
            base.MoveLeft(destination);
            if (XPos != destination)
            {
                base.MoveLeft(destination);
            }
        }      
        
        
    }
}
