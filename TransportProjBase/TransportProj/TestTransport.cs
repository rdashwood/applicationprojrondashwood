﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit;
using NUnit.Framework;

namespace TransportProj
{
    [TestFixture]
    class TestTransport
    {
        [Test]
        public void TestSedanPickUpPassengerNoMove()
        {
            var city = new City(10,10);
            var passenger = new Passenger(0, 0, 2, 2, city);
            var testSedan = new Sedan(0, 0, city, null);
            
            Assert.AreEqual(0,testSedan.XPos);
            Assert.AreEqual(0,testSedan.YPos);
            Assert.IsNull(testSedan.Passenger);

            Program.Tick(testSedan, passenger);
            Assert.IsNotNull(testSedan.Passenger);
        }
        
        [Test]
        public void TestSedanMoveRight()
        {
            var city = new City(10,10);
            var passenger = new Passenger(1, 1, 2, 2, city);
            var testSedan = new Sedan(0, 0, city, passenger);
            Assert.AreEqual(0,testSedan.XPos);
            Assert.AreEqual(0,testSedan.YPos);
            Program.Tick(testSedan, passenger);
            Assert.AreEqual(1,testSedan.XPos);
            Assert.AreEqual(0,testSedan.YPos);
        }        
        
        [Test]
        public void TestSedanMoveLeft()
        {
            var city = new City(10,10);
            var passenger = new Passenger(1, 1, 2, 2, city);
            var testSedan = new Sedan(5, 0, city, passenger);
           Program.Tick(testSedan, passenger);
            Assert.AreEqual(4,testSedan.XPos);
            Assert.AreEqual(0,testSedan.YPos);
        }       
        
        [Test]
        public void TestSedanMoveDown()
        {
            var city = new City(10,10);
            var passenger = new Passenger(1, 1, 2, 2, city);
            var testSedan = new Sedan(1, 0, city, null);
           Program.Tick(testSedan, passenger);
            Assert.AreEqual(1,testSedan.XPos);
            Assert.AreEqual(1,testSedan.YPos);
        }
       
        [Test]
        public void TestSedanPickUpPassengerMax()
        {
            var city = new City(10, 10);
            var passenger = new Passenger(9,9, 2, 2, city);
            var testSedan = new Sedan(0, 0, city, null);

            Assert.AreEqual(0, testSedan.XPos);
            Assert.AreEqual(0, testSedan.YPos);
            Assert.IsNull(testSedan.Passenger);

            while (testSedan.Passenger ==null)
            {
                Program.Tick(testSedan, passenger);
            }
            Assert.IsNotNull(testSedan.Passenger);
        }        
        
        [Test]
        public void TestSedanTakePassengerToDestination()
        {
            var city = new City(10, 10);
            var passenger = new Passenger(9,9, 2, 2, city);
            var testSedan = new Sedan(0, 0, city, null);

            Assert.AreEqual(0, testSedan.XPos);
            Assert.AreEqual(0, testSedan.YPos);
            Assert.IsNull(testSedan.Passenger);

            while (!passenger.IsAtDestination())
            {
                Program.Tick(testSedan, passenger);
            }
            Assert.IsNotNull(passenger.IsAtDestination());
        }        
        
        [Test]
        public void TestRacePickUpPassengerNoMove()
        {
            var city = new City(10,10);
            var passenger = new Passenger(0, 0, 2, 2, city);
            var testRace = new Race(0, 0, city, null);
            
            Assert.AreEqual(0,testRace.XPos);
            Assert.AreEqual(0,testRace.YPos);
            Assert.IsNull(testRace.Passenger);

            Program.Tick(testRace, passenger);
            Assert.IsNotNull(testRace.Passenger);
        }
        
        [Test]
        public void TestRaceMoveRightOne()
        {
            var city = new City(10,10);
            var passenger = new Passenger(1, 1, 2, 2, city);
            var testRace = new Race(0, 0, city, passenger);
            Assert.AreEqual(0,testRace.XPos);
            Assert.AreEqual(0,testRace.YPos);
            Program.Tick(testRace, passenger);
            Assert.AreEqual(2,testRace.XPos);
            Assert.AreEqual(0,testRace.YPos);
        }                
        
        [Test]
        public void TestRaceMoveRightTwo()
        {
            var city = new City(10,10);
            var passenger = new Passenger(3, 3, 2, 2, city);
            var testRace = new Race(0, 0, city, passenger);
            Assert.AreEqual(0,testRace.XPos);
            Assert.AreEqual(0,testRace.YPos);
            Program.Tick(testRace, passenger);
            Assert.AreEqual(2,testRace.XPos);
            Assert.AreEqual(0,testRace.YPos);
        }        
        
        [Test]
        public void TestRaceMoveLeft()
        {
            var city = new City(10,10);
            var passenger = new Passenger(1, 1, 2, 2, city);
            var testRace = new Race(5, 0, city, passenger);
           Program.Tick(testRace, passenger);
            Assert.AreEqual(3,testRace.XPos);
            Assert.AreEqual(0,testRace.YPos);
        }       
        
        [Test]
        public void TestRaceMoveDown()
        {
            var city = new City(10,10);
            var passenger = new Passenger(1, 2, 2, 2, city);
            var testRace = new Race(1, 0, city, null);
            Program.Tick(testRace, passenger);
            Assert.AreEqual(1,testRace.XPos);
            Assert.AreEqual(2,testRace.YPos);
        }
       
        [Test]
        public void TestRacePickUpPassengerMax()
        {
            var city = new City(10, 10);
            var passenger = new Passenger(9,9, 2, 2, city);
            var testRace = new Race(0, 0, city, null);

            Assert.AreEqual(0, testRace.XPos);
            Assert.AreEqual(0, testRace.YPos);
            Assert.IsNull(testRace.Passenger);

            while (testRace.Passenger ==null)
            {
                Program.Tick(testRace, passenger);
            }
            Assert.IsNotNull(testRace.Passenger);
        }        
        
        [Test]
        public void TestRaceTakePassengerToDestination()
        {
            var city = new City(10, 10);
            var passenger = new Passenger(9,9, 2, 2, city);
            var testRace = new Race(0, 0, city, null);

            Assert.AreEqual(0, testRace.XPos);
            Assert.AreEqual(0, testRace.YPos);
            Assert.IsNull(testRace.Passenger);

            while (!passenger.IsAtDestination())
            {
                Program.Tick(testRace, passenger);
            }
            Assert.IsNotNull(passenger.IsAtDestination());
        }
    }
}
